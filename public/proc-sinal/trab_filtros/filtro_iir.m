% Filtros IIR

% limpeza ...
clear all; 
close all;

Fs = 4000;
FsN = Fs/2;
NB = 4; %16; %256;  % 8 bits 1024;   %10 bits  4096;   %12 bits    16384;    % 14 bits   65536;  % 16 bits
tam = 512;   % valores p/ visualiza��o
tam2 = 256;
              
% define sen�ides
n = (1:100)/Fs;
f = 1000;  
sen = sin(2*pi*f*n); 
f2 = 1100;  
sen2 = sin(2*pi*f2*n); 

% define corte
fc_real = [990	1010];
fc2_real = [1090 1110];
fc = fc_real/FsN;			% faz a normaliza��o
fc2 = fc2_real/FsN;

% filtro butter
ord = 1;				% ordem do filtro
[N,D] = butter(ord, fc);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   

[y,t] = impz(N,D);
figure,plot(t,y),title('sa�da do butter (passa-faixa 1kHz)');
figure,freqz(N,D),title('resp. freq. do butter (passa-faixa 1kHz)');

y = filter(N,D,sen);
figure,plot(y),title('sa�da do filtro butter c/ sen�ide');
figure,freqz(y),title('resp. freq. filtro butter c/ sen�ide');

S = fft(sen,tam);
S2 = fft(sen2,tam);
w = (0:tam2-1)/tam2*FsN;
S1 = fft(y,tam);
figure, plot(w,abs([S(1:tam2)' S1(1:tam2)'])),title('resp. fourier filtro butter - 1KHz');

% filtro cheby1
ord = 1;				% ordem do filtro
[N,D] = cheby1(ord, 0.001, fc);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   

[y,t] = impz(N,D);
figure,plot(t,y),title('sa�da do cheby1 (passa-faixa 1kHz)');
figure,freqz(N,D),title('resp. freq. do cheby1 (passa-faixa 1kHz)');

y = filter(N,D,sen);
figure,plot(y),title('sa�da do filtro cheby1 c/ sen�ide 1kHz');
figure,freqz(y),title('resp. freq. filtro cheby1 c/ sen�ide 1kHz');

S1 = fft(y,tam);
figure, plot(w,abs([S(1:tam2)' S1(1:tam2)'])),title('resp. fourier filtro cheby1 - 1KHz');

[N,D] = cheby1(ord, 0.001, fc2);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   

[y,t] = impz(N,D);
figure,plot(t,y),title('sa�da do cheby1 (passa-faixa 1,1kHz)');
figure,freqz(N,D),title('resp. freq. do cheby1 (passa-faixa 1,1kHz)');

y = filter(N,D,sen2);
figure,plot(y),title('sa�da do filtro cheby1 c/ sen�ide 1,1kHz');
figure,freqz(y),title('resp. freq. filtro cheby1 c/ sen�ide 1,1kHz');

S1 = fft(y,tam);
figure, plot(w,abs([S2(1:tam2)' S1(1:tam2)'])),title('resp. fourier filtro cheby1 - 1,1KHz');


% filtro cheby2
ord = 1;				% ordem do filtro
[N,D] = cheby2(ord, 40, fc);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   

[y,t] = impz(N,D);
figure,plot(t,y),title('sa�da do cheby2 (passa-faixa 1kHz)');
figure,freqz(N,D),title('resp. freq. do cheby2 (passa-faixa 1kHz)');

y = filter(N,D,sen);
figure,plot(y),title('sa�da do filtro cheby2 c/ sen�ide 1kHz');
figure,freqz(y),title('resp. freq. filtro cheby2 c/ sen�ide 1kHz');

S1 = fft(y,tam);
figure, plot(w,abs([S(1:tam2)' S1(1:tam2)'])),title('resp. fourier filtro cheby2 - 1KHz');

% filtro elipse
ord = 1;				% ordem do filtro
[N,D] = ellip(ord, 0.001, 40, fc);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   

[y,t] = impz(N,D);
figure,plot(t,y),title('sa�da do ellip (passa-faixa 1kHz)');
figure,freqz(N,D),title('resp. freq. do ellip (passa-faixa 1kHz)');

y = filter(N,D,sen);
figure,plot(y),title('sa�da do filtro ellip c/ sen�ide 1kHz');
figure,freqz(y),title('resp. freq. filtro ellip c/ sen�ide 1kHz');

S1 = fft(y,tam);
figure, plot(w,abs([S(1:tam2)' S1(1:tam2)'])),title('resp. fourier filtro ellip - 1KHz');
