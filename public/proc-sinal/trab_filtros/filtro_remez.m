% Teste do comando remez

% limpeza ...
clear all; 
%close all;

Fs = 4000;
FsN = Fs/2;
              
% define o numero de taps
N = 470;			% 4KHz - calculado: 430

freq_real = [0     	960   	990      1010    1040   FsN];
ampl =      [0.001 0.001 1        1       0.001  0.001];

freq = freq_real/FsN;

b = remez(N,freq,ampl);
[h,w] = freqz(b,1,32);
figure, plot(freq,ampl,w/pi,abs(h)),title(N);
