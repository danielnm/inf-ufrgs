% Filtros FIR

% limpeza ...
clear all; 
close all;

Fs = 4000;
FsN = Fs/2;
NB = 16; %16384;    % 14 bits   65536;  % 16 bits
tam = 512;   % valores p/ visualiza��o
tam2 = 256;
              
% define o numero de taps
taps = 470;     % calculado: 430 
n = (1:100)/Fs;

% filtro 1KHz
freq_real = [0     	960   	990      1010    1040   FsN];
ampl =      [0.001	0.001		1        1       0.001  0.001];

% filtro 1,1KHz
freq2_real = [0     	1060  	1090   1110    1140		FsN];	 
ampl2 =      [0.001	0.001		1      1       0.001		0.001]; 	

freq = freq_real/FsN;	% faz a normaliza��o
freq2 = freq2_real/FsN;

f = 1000;  
sen = sin(2*pi*f*n); 
f2 = 1100;  
sen2 = sin(2*pi*f2*n); 
figure, plot(n,sen),title('sen�ide 1kHz');
figure, plot(n,sen2),title('sen�ide 1,1kHz');

coef = fir2(taps,freq,ampl,blackman(taps+1));
coef2 = fir2(taps,freq2,ampl2,blackman(taps+1));
coef1 = floor(coef*NB)/NB;
coef3 = floor(coef2*NB)/NB;

% compara blackman com hamming
coef_ham = fir2(taps,freq,ampl);
figure,freqz(coef),title('resp. freq. do blackman (passa-faixa 1kHz)');
figure,freqz(coef_ham),title('resp. freq. do hamming (passa-faixa 1kHz)');

figure,plot(coef1),title('coeficientes do blackman (passa-faixa 1kHz)');
figure,freqz(coef1),title('resp. freq. filtro blackman (passa-faixa 1kHz)');

figure,plot(coef3),title('coeficientes do blackman (passa-faixa 1,1kHz)');
figure,freqz(coef3),title('resp. freq. filtro blackman (passa-faixa 1,1kHz)');

y = filter(coef1,1,sen);
y2 = filter(coef3,1,sen2);

figure,plot(n,y),title('sa�da do blackman - sen�ide em 1KHz');
figure,freqz(y),title('resp. freq. blackman - sen�ide em 1KHz');

figure,plot(n,y2), title('sa�da do blackman - sen�ide em 1,1Hz');
figure,freqz(y2),title('resp. freq. blackman - sen�ide em 1,1Hz');

S = fft(sen,tam);
S1 = fft(y,tam);
S2 = fft(sen2,tam);
S3 = fft(y2,tam);

w = (0:tam2-1)/tam2*FsN;
figure,plot(w,abs([S(1:tam2)' S1(1:tam2)'])),title('resp. fourier filtro 1KHz');
figure,plot(w,abs([S2(1:tam2)' S3(1:tam2)'])),title('resp. fourier filtro 1,1KHz');

