% Filtros no dom�nio freq��ncia

% limpeza ...
clear all; 
close all;

Fs = 4000;
FsN = Fs/2;
NB = 256;  % 8 bits %16384;    % 14 bits   65536;  % 16 bits	
tam = 512;   % valores p/ visualiza��o
tam2 = 256;

%----------------------------------
% FIR

% define o numero de taps
taps = 20;     % calculado: 430 
n = (1:100)/Fs;

% filtro 1KHz
freq_real = [0     	960   	990      1010    1040   FsN];
ampl =      [0.001	0.001		1        1       0.001  0.001];

% filtro 1,1KHz
freq2_real = [0     	1060  	1090   1110    1140		FsN];	 
ampl2 =      [0.001	0.001		1      1 	   0.001		0.001]; 	

freq = freq_real/FsN;	% faz a normaliza��o
freq2 = freq2_real/FsN;

f = 1000;  
sen = sin(2*pi*f*n); 
f2 = 1100;  
sen2 = sin(2*pi*f2*n); 
w = (0:tam2-1)/tam2*FsN;

coef = fir2(taps,freq,ampl,blackman(taps+1));
coef2 = fir2(taps,freq2,ampl2,blackman(taps+1));
coef1 = floor(coef*NB)/NB;
coef3 = floor(coef2*NB)/NB;

sen_freq = fft(sen,tam);
sen_freq2 = fft(sen2,tam);

filtro = fft(coef1,tam);
sinal_freq = sen_freq.*filtro; % note a multiplicacao de numeros complexos
sen_t = real(ifft(sinal_freq));
figure, plot(sen_t(1:tam2)),title('resultado no dom. tempo');

filtro = fft(coef3,tam);
sinal_freq2 = sen_freq2.*filtro; 
figure, plot(w,abs(sinal_freq(1:tam2)),'g'),
hold, plot(w,abs(sinal_freq2(1:tam2)),'r'),title('modulo da filtragem FIR - 1 x 1,1KHz');

sen_t = real(ifft(sinal_freq2));
figure, plot(sen_t(1:tam2)),title('resultado no dom. tempo');

%--------------------------------------------
%IIR

% define corte
fc_real = [990	1010];
fc2_real = [1090 1110];
fc = fc_real/FsN;			% faz a normaliza��o
fc2 = fc2_real/FsN;

% filtro cheby1
ord = 1;				% ordem do filtro
[N,D] = cheby1(ord, 0.001, fc);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   
[y,t] = impz(N,D);

filtro = fft(y,tam);
sinal_freq = sen_freq.*filtro(1:tam)'; 
sen_t = real(ifft(sinal_freq));
figure, plot(sen_t(1:tam2)),title('resultado no dom. tempo');

[N,D] = cheby1(ord, 0.001, fc2);
N = floor(N*NB)/NB;  
D = floor(D*NB)/NB;   
[y,t] = impz(N,D);
filtro = fft(y,tam);
sinal_freq2 = sen_freq2.*filtro(1:tam)'; 
figure, plot(w,abs(sinal_freq(1:tam2)),'g');
hold, plot(w,abs(sinal_freq2(1:tam2)),'r'),title('modulo da filtragem IIR - 1 x 1,1KHz');

sen_t = real(ifft(sinal_freq2));
figure, plot(sen_t(1:tam2)),title('resultado no dom. tempo');


