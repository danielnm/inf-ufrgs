function [y,e,h] = LMS(x,h,r,u) 
%LMS	LMS Adaptive Digital filter.
%	[Y,E,Hf] = LMS(X,H,R,u) runs the adaptive filter starting with
%	the coefficient vector H. Vector X is the data applied 
%	to the filter input. Vector R contains the reference 
%	data. The filter is a "Direct Form I" implementation 
%	of the standard difference equation:
% 
%	y(n) = h(1)*x(n) + h(2)*x(n-1) + ... + h(nh)*x(n-nh+1)
%	
%	u is the step size factor. E is the error vector defined by
%	e(n) = r(n) - y(n) 
%	
%	Last updated 27/dez/1996 by Negreiros


%       comentarios internos 
% 	x e' o vetor de entrada do filtro
% 	h e' o vetor de coeficientes
% 	y e a saida do filtro
% 	d e' o vetor de linha de atraso
% 	i e' o iterador
% 	n e' o numero de coeficientes
%	r e' o vetor de referencia
%	e e' o vetor de erro

% inicializacoes
y=[];
e=[];

if length(x)~=length(r)    
   error('Vectors X and R must have the same size.');
end

if length(x)>=length(h)
   % se o vetor de dados for maior ou igual que o de coef.
   n = length(h);           % comprimento da linha de atraso
   d = zeros(1,n);          % valor inicial da linha de atraso
   for i=1:length(x)
      d(1)   = x(i);        % nova amostra na linha de atraso
      y(i)   = h*d';        % calcula saida
      e(i)   = r(i) - y(i); % calcula erro atual
      h      = h + u*e(i)*d;% atualiza coeficientes via LMS
      d(2:n) = d(1:n-1);    % desloca amostras na linha de atraso
   end
else
   error('Vectors H cannot be greater than X.');
end
